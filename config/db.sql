CREATE TABLE admins(
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    username INT(60) NOT NULL,
    kata_sandi VARCHAR(60) NOT NULL,
    nama_lengkap VARCHAR(60) NOT NULL
);

CREATE TABLE kategori_kamar(
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(50) NOT NULL
);

CREATE TABLE kamar(
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    nama VARCHAR(50),
    kategori_kamar_id INT(11) NOT NULL,
    FOREIGN KEY (kategori_kamar_id) REFERENCES kategori_kamar(id),
    status INT(1) NOT NULL
);

CREATE TABLE gambar_kamar(
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    nama varchar(100) NOT NULL,
    kamar_id INT(11) NOT NULL,
    FOREIGN KEY (kamar_id) REFERENCES kategori_kamar(id)
);

CREATE TABLE kategori_menu(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(50) NOT NULL
);

CREATE TABLE menu(
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    cover TEXT NOT NULL,
    label VARCHAR(50) NOT NULL,
    kategori_menu_id INT(11) NOT NULL,
    FOREIGN KEY (kategori_menu_id) REFERENCES kategori_menu(id),
    harga INT(8) NOT NULL,
    deskripsi TEXT NOT NULL
);

CREATE TABLE gambar_menu(
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    nama varchar(100) NOT NULL,
    menu_id INT(11) NOT NULL,
    FOREIGN KEY (menu_id) REFERENCES menu(id)
);

CREATE TABLE tamu(
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    nama_lengkap VARCHAR(100) NOT NULL,
    nomer_telepon VARCHAR(13) NOT NULL,
    email VARCHAR(100) NOT NULL,
    jenis_kelamin INT(1) NOT NULL,
    kata_sandi VARCHAR(60) NOT NULL,
    alamat TEXT,
    ktp VARCHAR(16) NOT NULL,
    tempat_tanggal_lahir VARCHAR(100),
    isFlag INT(1)
);

CREATE TABLE reservasi(
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    tamu_id INT(11) NOT NULL,
    FOREIGN KEY (tamu_id) REFERENCES tamu(id),
    kamar_id INT(11) NOT NULL,
    FOREIGN KEY (kamar_id) REFERENCES kamar(id),
    tanggal_reservasi DATETIME,
    catatan TEXT,
    status INT(1) NOT NULL
);

CREATE TABLE keranjang(
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    menu_id INT(11) NOT NULL,
    FOREIGN KEY (menu_id) REFERENCES menu(id),
    qty INT(3) NOT NULL,
    catatan TEXT,
    tamu_id INT(11) not null,
    FOREIGN KEY (tamu_id) REFERENCES tamu(id)
);

CREATE TABLE pembayaran(
    invoice VARCHAR(25) PRIMARY KEY,
    tamu_id INT(11) NOT NULL,
    FOREIGN KEY (tamu_id) REFERENCES tamu(id),
    estimasi TIME NULL,
    status INT(1) NOT NULL,
    createdAt DATETIME
);

CREATE TABLE detail_pembelian(
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    pembayaran_id VARCHAR(25),
    FOREIGN KEY(pembayaran_id) REFERENCES pembayaran(invoice),
    label VARCHAR(50) NOT NULL,
    qty INT(3) NOT NULL,
    harga INT(8) NOT NULL,
    catatan TEXT,
    isFlag INT(1)
);