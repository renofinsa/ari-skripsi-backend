const mysql = require('mysql2');
const ENV = require('./../../config/env.json')

const connection = mysql.createConnection(ENV.prod)
module.exports = connection
