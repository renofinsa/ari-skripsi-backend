const express = require('express')
const r = express.Router()
const connection = require('./../util/index')
const moment = require('moment')
const randomInt = require('random-int');



r.get('/paid/history',(req,res) => {
    connection.query(`SELECT a.pembayaran_id as invoice,
    c.nama_lengkap, c.nomer_telepon, b.createdAt,b.status,
    SUM(a.qty*a.harga) as total, SUM(a.qty) as total_qty FROM detail_pembelian a
    LEFT JOIN pembayaran b ON a.pembayaran_id = b.invoice
    LEFT JOIN tamu c ON b.tamu_id= c.id
    GROUP BY a.pembayaran_id
    ORDER BY b.createdAt DESC`, (
    (err, results, fields) => {
        var payload = []
        results.map(a => {
            var ss = ''
            if(a.status == 1){
                ss = 'Belum Lunas'
            }else{
                ss = 'Lunas'
            }
            var dt = moment(a.createdAt).format('hh:mm DD-MM-YYYY')
            payload.push({
                invoice: a.invoice,
                nama_lengkap: a.nama_lengkap,
                nomer_telepon: a.nomer_telepon,
                total_harga: a.total,
                total_qty: a.total_qty,
                createdAt: dt,
                status: ss
            })
        })
        if(results == 0){
            res.json({
                success: false,
                message: 'terjadi kesalahan!',
                data: payload
            })
        }else{
            res.status(200).json({
                success: true,
                message: 'Riwayat Pembayaran!',
                data: payload
            })
        } 
    }))
})

r.get('/paid/history/:id',(req,res) => {
    var id = req.params.id
    connection.query(`SELECT a.pembayaran_id as invoice,
    c.nama_lengkap, c.nomer_telepon, b.createdAt,b.status,
    SUM(a.qty*a.harga) as total, SUM(a.qty) as total_qty FROM detail_pembelian a
    LEFT JOIN pembayaran b ON a.pembayaran_id = b.invoice
    LEFT JOIN tamu c ON b.tamu_id= c.id
    WHERE c.id = ${id}
    GROUP BY a.pembayaran_id
    ORDER BY b.createdAt DESC`, (
    (err, results, fields) => {
        var payload = []
        results.map(a => {
            var ss = ''
            if(a.status == 1){
                ss = 'Belum Lunas'
            }else{
                ss = 'Lunas'
            }
            var dt = moment(a.createdAt).format('hh:mm DD-MM-YYYY')
            payload.push({
                invoice: a.invoice,
                nama_lengkap: a.nama_lengkap,
                nomer_telepon: a.nomer_telepon,
                total_harga: a.total,
                total_qty: a.total_qty,
                createdAt: dt,
                status: ss
            })
        })
        if(results == 0){
            res.json({
                success: false,
                message: 'terjadi kesalahan!',
                data: payload
            })
        }else{
            res.status(200).json({
                success: true,
                message: 'Riwayat Pembayaran!',
                data: payload
            })
        } 
    }))
})

r.get('/list',(req,res) => {
    var id = req.params.id
    connection.query(`SELECT b.invoice,c.nama_lengkap,c.nomer_telepon,a.label, a.qty,a.catatan, b.estimasi FROM detail_pembelian a
    LEFT JOIN pembayaran b ON a.pembayaran_id = b.invoice
    LEFT JOIN tamu c ON b.tamu_id = c.id
    WHERE a.isFlag = 1
     `, (
        (err, results, fields) => {
            var payload = []
            results.map(a => {
                var bb = moment('00:00:00').format('h:mm:ss')
                console.log(bb)
                var es = ''
                if(a.estimasi == '00:00:00'){
                    es = 'Tanpa Estimasi'
                }else{
                    es = a.estimasi
                }
                payload.push({
                    invoice         : a.invoice,
                    nama_lengkap   : a.nama_lengkap,
                    nomer_telepon   : a.nomer_telepon,
                    label           : a.label,
                    qty             : a.qty,
                    catatan         : a.catatan,
                    estimasi        : es,
                })
            })
            
            console.log(results)
            console.log(fields)
            if(results == 0){
                
                res.json({
                    success: false,
                    message: 'data tidak tersedia!',
                    data:payload

                })
            }else{
                
                return res.status(200).json({
                    success: true,
                    data:payload
                })
            } 
        }))
})

r.post('/add/cart',(req,res) => {
    var {
        menu_id,
        qty,
        catatan,
        tamu_id
    } = req.body

    connection.query(`INSERT INTO keranjang VALUES (
        NULL,
        '${menu_id}',
        '${qty}',
        '${catatan}',
        '${tamu_id}'  
        )`,
    (err, results, fields) => {
        console.log(err)
        console.log(results)
        console.log(fields)
        if(err){
            res.json({
                success: false,
                message: 'terjadi kesalahan'
            })
        }else{
            res.json({
                success: true,
                message: 'berhasil ditambah'
            })
        }
    })

})

r.get('/cart/list/:id',(req,res) => {
    var id = req.params.id
    connection.query(`SELECT a.qty*b.harga as total_harga_item, a.id, b.label as name, a.qty, a.catatan as note, c.nama_lengkap as person, c.id as person_id FROM keranjang a
    LEFT JOIN menu b ON a.menu_id = b.id
    LEFT JOIN tamu c ON a.tamu_id = c.id
    WHERE a.tamu_id = '${id}'
     `, (
        (err, results, fields) => {
            let payload = []
            results.map(a => {
                payload.push(a)
            })            
            if(results == 0){
                res.json({
                    success: false,
                    message: 'data tidak tersedia!'
                })
            }else{
                return res.status(200).json({
                    success: true,
                    data:payload
                })
            } 
        }))
})

r.post('/add', async(req,res) => {
    var {
        tamu_id,
        estimasi,
    } = req.body

    var dd = new Date()
    var getdd = moment(dd).format('DDMMYYYYHH')
    var randomId = randomInt(1, 9999);
    const invoice_number = `INV_${randomId}_${getdd}`
    console.log(invoice_number)
    
    connection.query(`SELECT a.*, b.harga, b.label as name_menu FROM keranjang a
    LEFT JOIN menu b ON a.menu_id = b.id
    WHERE a.tamu_id = '${tamu_id}'`,
    (
        (err, results, fields) => {
            let payload = []
            results.map(a => {
                payload.push(a)
            })            
            if(results == 0){
                res.json({
                    success: false,
                    message: 'data tidak tersedia!'
                })
            }else{
                connection.query(`INSERT INTO pembayaran VALUES(
                    '${invoice_number}',
                    '${tamu_id}',
                    '${estimasi}',
                    1, NULL)`,
                (
                    (err, results, fields) => {
                        var i;
                        for (i = 0; i < payload.length; i++) { 
                            console.log(payload)
                            connection.query(`INSERT INTO detail_pembelian VALUES(
                                NULL,
                                '${invoice_number}',
                                '${payload[i].name_menu}',
                                '${payload[i].qty}',
                                '${payload[i].harga}',
                                '${payload[i].catatan}',
                                1
                                )`,
                                )
                            connection.query(`DELETE FROM keranjang WHERE tamu_id = '${tamu_id}' `
                                )
                            }
                            console.log('------------------------------------')
                            console.log(payload.length)
                            console.log('------------------------------------')
                            res.json({
                            success: true,
                            message: 'berhasil memesan!'
                        })
                    })

                )
            } 
        })
    )
})

r.delete('/cart/delete/:id', (req,res) => {
    var id = req.params.id
    connection.query(`DELETE FROM keranjang WHERE id = ${id}`,(
        (err, results, fields) => {
            console.log(err)
            console.log(results)
            console.log(fields)
            if(results.affectedRows == 0){
                res.status(400).json({
                    success: false,
                    message: 'data tidak ditemukan'
                })
            }else{
                res.status(200).json({
                    success: true,
                    message: 'berhasil dihapus'
                })
            }
        }
    ))

})

r.get('/',(req,res) => {
    connection.query(`
    SELECT a.*, SUM(b.qty*b.harga)as total FROM pembayaran a
LEFT JOIN detail_pembelian b ON a.invoice = b.pembayaran_id
GROUP BY a.invoice
     `, (
        (err, results, fields) => {
            if(results == 0){
                res.json({
                    success: false,
                    message: 'data tidak tersedia!'
                })
            }else{
                var payload = []
                results.map(a => {
                    var aa = ''
                    if(a.estimasi == '00:00:00'){
                        aa = 'Tanpa estimasi'
                    }else{
                        aa = a.estimasi
                    }

                    payload.push({
                        invoice     : a.invoice,
                        tamu_id     : a.tamu_id,
                        estimasi    : aa,
                        status      : a.status,
                        harga_total : a.total
                    })
                })
                
                return res.status(200).json({
                    success: true,
                    data:payload
                })
            } 
        }))
})

r.get('/user/:id',(req,res) => {
    var id = req.params.id
    connection.query(`
    SELECT a.*, SUM(b.qty*b.harga)as total FROM pembayaran a
    LEFT JOIN detail_pembelian b ON a.invoice = b.pembayaran_id
    WHERE a.tamu_id = '${id}'
    GROUP BY a.invoice
     `, (
        (err, results, fields) => {
            if(results == 0){
                res.json({
                    success: false,
                    message: 'data tidak tersedia!'
                })
            }else{
                var payload = []
                results.map(a => {
                    var aa = ''
                    if(a.estimasi == '00:00:00'){
                        aa = 'Tanpa estimasi'
                    }else{
                        aa = a.estimasi
                    }

                    payload.push({
                        invoice     : a.invoice,
                        tamu_id     : a.tamu_id,
                        estimasi    : aa,
                        status      : a.status,
                        harga_total : a.total
                    })
                })
                
                return res.status(200).json({
                    success: true,
                    data:payload
                })
            } 
        }))
})

r.get('/user/bill/:id',(req,res) => {
    var id = req.params.id
    connection.query(`
    SELECT a.*, SUM(b.qty*b.harga)as total FROM pembayaran a
    LEFT JOIN detail_pembelian b ON a.invoice = b.pembayaran_id
    WHERE a.tamu_id = '${id}' and a.status = 1
    GROUP BY a.invoice
     `, (
        (err, results, fields) => {
            if(results == 0){
                res.json({
                    success: false,
                    message: 'data tidak tersedia!'
                })
            }else{
                var payload = []
                results.map(a => {
                    var aa = ''
                    if(a.estimasi == '00:00:00'){
                        aa = 'Tanpa estimasi'
                    }else{
                        aa = a.estimasi
                    }

                    payload.push({
                        invoice     : a.invoice,
                        tamu_id     : a.tamu_id,
                        estimasi    : aa,
                        status      : a.status,
                        harga_total : a.total
                    })
                })
                
                return res.status(200).json({
                    success: true,
                    data:payload
                })
            } 
        }))
})

// detail pembelian
r.get('/:id',(req,res) => {
    var id = req.params.id
    connection.query(`SELECT * FROM detail_pembelian WHERE pembayaran_id = '${id}'
     `, (
        (err, results, fields) => {
            var payload = []
            
            if(results == 0){
                console.log(err)
            console.log(results)
                res.json({
                    success: false,
                    message: 'data tidak tersedia!',
                    data:payload

                })
            }else{
                results.map(a => {
                    var total_harga_item = a.qty * a.harga
                    payload.push({
                        id              : a.id,
                        pembayaran_id   : a.pembayaran_id,
                        label           : a.label,
                        qty             : a.qty,
                        harga_satuan    : a.harga,
                        total_harga_item: total_harga_item,
                        catatan         : a.catatan
                    })
                })
                
                return res.status(200).json({
                    success: true,
                    data:payload
                })
            } 
        }))
})


r.get('/unpaid/user/:id',(req,res) => {
    var id = req.params.id
    connection.query(`SELECT a.*, SUM(b.qty*b.harga) as total_harga, SUM(b.qty) as total_qty FROM pembayaran a
    LEFT JOIN detail_pembelian b ON a.invoice = b.pembayaran_id
    WHERE a.tamu_id = '${id}' AND a.status = 1
    GROUP BY a.invoice
    `, (
    (err, results, fields) => {
        var payload = []
        
        if(results == 0){
            res.json({
                success: false,
                message: 'terjadi kesalahan!',
                data: payload

            })
        }else{
            results.map(a => {
                var getStatus = ''
                if(a.status == 0){
                    getStatus = 'Lunas'
                }else{
                    getStatus = 'Belum Lunas'
                }
                payload.push({
                    invoice: a.invoice,
                    status: getStatus,
                    total_harga: a.total_harga,
                    total_qty: a.total_qty
                })
            })
            res.status(200).json({
                success: true,
                message: 'Invoice Pelanggan yang harus di bayar',
                data: payload
            })
        } 
    }))
})

r.get('/unpaid/user/total/:id',(req,res) => {
    var id = req.params.id
    connection.query(`SELECT SUM(b.harga) as total_harga, SUM(b.qty) as total_qty FROM pembayaran a
    LEFT JOIN detail_pembelian b ON a.invoice = b.pembayaran_id
    WHERE a.tamu_id = '${id}' AND a.status = 1
    `, (
    (err, results, fields) => {
        var payload = []
        
        if(results == 0){
            res.json({
                success: false,
                message: 'terjadi kesalahan!',
                data: payload

            })
        }else{
            results.map(a => {
                var getStatus = ''
                if(a.status == 0){
                    getStatus = 'Lunas'
                }else{
                    getStatus = 'Belum Lunas'
                }
                payload.push({
                    total_harga: a.total_harga,
                    total_qty: a.total_qty
                })
            })
            res.status(200).json({
                success: true,
                message: 'Invoice Pelanggan yang harus di bayar',
                data: payload
            })
        } 
    }))
})

module.exports = r
