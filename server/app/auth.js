const express = require('express')
const r = express.Router()
const connection = require('./../util/index')
const randomInt = require('random-int');
const fs = require('fs')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const SECRET = 'asjd9x8cuz9xiuc01id9ixmozicn982klkczkc'


r.post('/register', async (req,res, next) => {
    
    var {
      nama_lengkap,
      nomer_telepon,
      email,
      jenis_kelamin,
      kata_sandi,
      alamat,
      ktp,
      tempat_tanggal_lahir,
    } = req.body
    
    // validasi email
    connection.query(`SELECT * FROM tamu WHERE email = '${email}' `,
    (err, results, fields) => {
        console.log(results)
        if(results == 0){ // kondisi jika email tidak tersedia
             bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(kata_sandi, salt, (err, hash) =>{
                    kata_sandi = hash
                    connection.query(`INSERT INTO tamu VALUES(NULL,
                    '${nama_lengkap}',
                    '${nomer_telepon}',
                    '${email}',
                    '${jenis_kelamin}',
                    '${kata_sandi}',
                    '${alamat}',
                    '${ktp}',
                    '${tempat_tanggal_lahir}',
                    '0'
                    ) `,
                    (err, results, fields) => {
                        res.status(200).json({
                            success: true,
                            message: 'tamu berhasil terdaftar'
                        })
                    })
                })
            })
        }else{ // kondisi jika email tersedia
            res.json({
                success: false,            
                message: 'Email sudah terdaftar!'
            })
        }
    }) 
})

r.post('/login',(req,res) => {
    var {
        email, 
        kata_sandi,
    } = req.body
    connection.query(`SELECT * FROM tamu WHERE email = '${email}' `,
    (err, results, fields) => {
        if(results == 0){ // kondisi jika email tidak tersedia
            res.json({message: 'Email anda salah!'})
        }else{ // kondisi jika email tersedia
            var tamu = results[0]
            console.log(tamu.kata_sandi)
            if (bcrypt.compareSync(kata_sandi, tamu.kata_sandi)){
                var payload = {
                  email: tamu.email,
                  userId: tamu.id
                }
                var token = jwt.sign(payload, SECRET)
                res.cookie('_token', token, {
                    httpOnly: true,
                    signed: true,
                  })

                  var data = {
                    id                  : tamu.id,
                    nama_lengkap        : tamu.nama_lengkap,
                    nomer_telepon       : tamu.nomer_telepon,
                    email               : tamu.email,
                    jenis_kelamin       : tamu.jenis_kelamin,
                    alamat              : tamu.alamat,
                    ktp                 : tamu.ktp,
                    tempat_tanggal_lahir: tamu.tempat_tanggal_lahir,
                  }


                  res.status(200).json({ 
                    success: true,
                    message: 'login berhasil',
                    data: data
                  })
            }else{
                res.status(401).json({ 
                    success: false,
                    message: 'password salah!',
                  })
            }
        }
    }) 
})

module.exports = r