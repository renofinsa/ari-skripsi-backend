const express = require('express')
const r = express.Router()
const connection = require('./../util/index')
const moment = require('moment')

r.get('/', (req, res) => {
    res.send('connected')
})

r.get('/tamu',(req,res) => {
    connection.query(`
        SELECT * FROM tamu
    `,(err, results, fields) => {
        var payload = []
            results.map(a => {
                var gender = ''
                if(a.jenis_kelamin == 1){
                    gender = 'Pria'
                }else{
                    gender = 'Wanita'
                }
                payload.push({
                    id                  : a.id,
                    nama_lengkap        : a.nama_lengkap,
                    nomer_telepon       : a.nomer_telepon,
                    email               : a.email,
                    jenis_kelamin       : gender,
                    alamat              : a.alamat,
                    ktp                 : a.ktp,
                    tempat_tanggal_lahir: a.tempat_tanggal_lahir,
                })
            })
        if(results == 0){
            res.json({
                success: false,                
                message: 'data tidak tersedia!',
                data:payload

            })
        }else{
            

            return res.status(200).json({
                success: true,                    
                data:payload
            })
        } 
    })
})

r.get('/tamu/tersedia',(req,res) => {
    connection.query(`
        SELECT * FROM tamu
        WHERE isFlag = 0
    `,(err, results, fields) => {
        var payload = []
            results.map(a => {
                var gender = ''
                if(a.jenis_kelamin == 1){
                    gender = 'Pria'
                }else{
                    gender = 'Wanita'
                }
                payload.push({
                    id                  : a.id,
                    nama_lengkap        : a.nama_lengkap,
                    nomer_telepon       : a.nomer_telepon,
                    email               : a.email,
                    jenis_kelamin       : gender,
                    alamat              : a.alamat,
                    ktp                 : a.ktp,
                    tempat_tanggal_lahir: a.tempat_tanggal_lahir,
                })
            })
        if(results == 0){
            res.json({
                success: false,                
                message: 'data tidak tersedia!',
                data:payload

            })
        }else{
            

            return res.status(200).json({
                success: true,                    
                data:payload
            })
        } 
    })
})

r.get('/reservasi/list',(req,res) => {
    connection.query(`SELECT a.id, b.nama_lengkap,a.tamu_id, c.nama as nomer_kamar, a.tanggal_reservasi, a.catatan, a.status FROM reservasi a
    LEFT JOIN tamu b ON a.tamu_id = b.id 
    LEFT JOIN kamar c ON a.kamar_id = c.id 
    WHERE a.status = 1 `, (
        (err, results, fields) => {
            let payload = []
            results.map(a => {
                payload.push({
                    id                  : a.id,
                    tamu_id             : a.tamu_id,
                    nama_lengkap        : a.nama_lengkap,
                    nomer_kamar         : a.nomer_kamar,
                    tanggal_reservasi   : moment(a.tanggal_reservasi).format('dd MMMM YYYY, h:mm a'),
                    catatan             : a.catatan,
                    status              : a.status
                })
            })            
            if(results == 0){
                res.json({
                    success: false,                
                    message: 'data tidak tersedia!',
                    data:payload

                })
            }else{
                return res.status(200).json({
                    success: true,                    
                    data:payload
                })
            } 
        }))
})

module.exports = r
