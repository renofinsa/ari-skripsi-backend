const express = require('express')
const r = express.Router()
const connection = require('./../util/index')
const moment = require('moment')

r.post('/',(req,res) => {
    var {
    tamu_id,
    kamar_id,
    catatan,
    } = req.body
    console.log(tamu_id)
    console.log(kamar_id)
    console.log(catatan)
    var date = new Date()
    var tanggal_reservasi = moment(date).format('YYYY-MM-DD h:mm:ss')

    connection.query(`INSERT INTO reservasi VALUES (
        NULL,
        '${tamu_id}',
        '${kamar_id}',
        '${tanggal_reservasi}',
        '${catatan}',
        '1') `,
    (err, results, fields) => {
        console.log('---simpan---')
        console.log(err)
        console.log(results)
        console.log(fields)
        if(err){
            console.log(err)
            res.json({
                success: false,
                message: 'terjadi kesalahan',
                response: err
            })
        }else{
            connection.query(`UPDATE kamar SET status = 1 WHERE id = '${kamar_id}' `, (
                (err, results, fields) => {
        console.log('---update---')

                    console.log(err)
                    console.log(results)
                    console.log(fields)
                    connection.query(`UPDATE tamu SET isFlag = 1 WHERE id = '${tamu_id}' `)
                    res.json({
                        success: true,
                        message: 'reservasi berhasil'
                    })
                }))
        }
    })

})

r.get('/detail/:id',(req,res) => {
    var id = req.params.id
    connection.query(`SELECT a.id, b.nama_lengkap, c.nama as nomer_kamar, a.tanggal_reservasi, a.catatan, a.status FROM reservasi a
    LEFT JOIN tamu b ON a.tamu_id = b.id 
    LEFT JOIN kamar c ON a.kamar_id = c.id 
    WHERE a.id = '${id}' `, (
        (err, results, fields) => {
            let payload = []
            results.map(a => {
                payload.push({
                    id                  : a.id,
                    nama_lengkap        : a.nama_lengkap,
                    nomer_kamar         : a.nomer_kamar,
                    tanggal_reservasi   : moment(a.tanggal_reservasi).format('dd MMMM YYYY, h:mm a'),
                    catatan             : a.catatan,
                    status              : a.status
                })
            })            
            if(results == 0){
                res.json({
                    success: false,
                    message: 'data tidak tersedia!'
                })
            }else{
                return res.status(200).json({
                    success: true,
                    data:payload
                })
            } 
        }))
})

r.patch('/paid/:id',  (req,res) => {
    var id = req.params.id
    var data = {}

    connection.query(`SELECT a.id,a.tamu_id,a.kamar_id, a.tanggal_reservasi FROM reservasi a
    LEFT JOIN tamu b ON a.tamu_id = b.id
    LEFT JOIN kamar c ON a.kamar_id = c.id
    LEFT JOIN kamar d ON a.tamu_id = d.id
    WHERE a.tamu_id = '${id}'
    ORDER BY a.id DESC
    LIMIT 1
    `, (
    (err, results, fields) => {
        data = {
            id : results[0].id,
            tamu_id : results[0].tamu_id,
            kamar_id : results[0].kamar_id,
            date : moment(results[0].tanggal_reservasi).format("YYYY-MM-DD hh:mm:ss"),
        }
        console.log(data)
        if(results == 0){
            res.json({message: 'terjadi kesalahan.'})
        }else{
            connection.query(`UPDATE tamu SET isFlag = 0 WHERE id = '${data.tamu_id}' `)
            connection.query(`UPDATE kamar SET status = 0 WHERE id = '${data.kamar_id}' `)
            connection.query(`UPDATE reservasi SET status = 0 WHERE id = '${data.id}' AND tanggal_reservasi = '${data.date}' `)
            // detail pembelian
            connection.query(`SELECT invoice FROM pembayaran WHERE tamu_id ='${data.tamu_id}' AND status = 1 `,(
                (err, results, fields) => {
                    var dataInvoice = []
                    results.map(a => {
                        dataInvoice.push({
                            invoice : a.invoice
                        })
                    })
                    // console.log('------pembayaran--------')
                    // console.log(dataInvoice)
                    // console.log(err)
                    // console.log(results)
                    // console.log(fields)
                    // console.log('------pembayaran--------')
                    
                        for(i = 0; i < dataInvoice.length; i++) { 
                            console.log(dataInvoice[i])
                             connection.query(`UPDATE detail_pembelian SET isFlag = 0
                             WHERE pembayaran_id = '${dataInvoice[i].invoice}' `)
                        }
                        var dd = new Date()
                        var gtdd = moment(dd).format('YYYY-MM-DD h:mm:ss')
                        for(i = 0; i < dataInvoice.length; i++) { 
                            console.log(dataInvoice[i])
                             connection.query(`UPDATE pembayaran SET status = 0, createdAt = '${gtdd}'
                             WHERE invoice = '${dataInvoice[i].invoice}' `)
                        }
                        res.json({
                            success: true,
                            message: 'checkout success'
                        })
                }))
        }
    }))
})

r.patch('/checkout/:id',(req,res) => {
    var id = req.params.id
    connection.query(`UPDATE reservasi SET status = 0
    WHERE id = '${id}' `, (
        (err, results, fields) => {
            if(results == 0){
                res.json({
                    success: false,
                    message: 'terjadi kesalahan!'
                })
            }else{
                res.json({
                    success: true,
                    message: 'checkout berhasil!'
                })
            } 
        }))

})



module.exports = r
