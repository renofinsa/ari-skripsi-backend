const express = require('express')
const r = express.Router()
const connection = require('./../util/index')
const randomInt = require('random-int');
const fs = require('fs')

//menu
r.get('/', (req, res) => {
        connection.query(`SELECT a.id, a.*, b.label as kategori FROM menu as a 
        LEFT JOIN kategori_menu as b ON a.kategori_menu_id = b.id 
        ORDER BY a.label `,
        (err, results, fields) => {
            let payload = []
            results.map(a => {
                payload.push(a)
            })            
            if(err){
                res.json({
                    success: false,
                    message: 'Not Found!'
                })
            }else{
                return res.status(200).json({
                    success: true,
                    path: '/images/menu/',
                    data:payload
                })
            }
        })
   
})

r.get('/detail/:id', async (req, res) => {``
    var id = req.params.id

    await connection.query(`SELECT * FROM menu WHERE id ='${id}' `,
    (err, results, fields) => {
            if(results == 0){
                res.json({
                    success: false,
                    message: '404'
                })
            }else{
            connection.query(`SELECT a.*, b.id as rls_id, b.label as rls_label FROM menu as a LEFT JOIN kategori_menu as b ON a.kategori_menu_id = b.id WHERE a.id = '${id}' `,
            (err, results, fields) => {
                console.log(results)
                var a = results[0]
                var payload = {
                    id          : a.id,
                    label       : a.label,
                    kategori    : a.rls_label,
                    kategori_id : a.kategori_menu_id,
                    harga       : a.harga,
                    deskripsi   : a.deskripsi,
                    cover   : a.cover,
                }
                
                if(results == 0){
                    res.json({
                        success: false,
                        message: 'Not Found!'})
                }else{
                    res.send({
                        success: true,
                        path: '/images/menu/',
                        data:payload
                    })
                }
            }) 
        }
    })
})

r.post('/', (req,res) => {

    var {
        label,
        kategori_menu_id,
        harga,
        deskripsi,
    } = req.body
    var cover = 'menu_cover.jpg'
    var randomId = randomInt(1, 9999);
    const getCover = req.files.file
    const coverImage = getCover.name
    var getName = `${randomId}_${coverImage.toLowerCase().replace(/ /g,'_')}`


    connection.query(`INSERT INTO menu 
    VALUES(NULL,'${getName}','${label}','${kategori_menu_id}','${harga}','${deskripsi}')`,
    (err, results, fields) => {
        if(err){
            res.json({
                success: false,
                message: 'failed'
            })
        }else{
            getCover.mv('./server/img/menu/' + getName)
            res.json({
                success: true,
                message: 'success'
            })
        }
    }) 

})

r.patch('/:id', async (req, res) => {
    var id = req.params.id
    var {
        label,
        kategori_menu_id,
        harga,
        deskripsi,
        oldCover
    } = req.body

    var randomId = randomInt(1, 9999);
    const getCover = ''
    console.log(getCover)
    var getName = ''
    const coverImage = ''
    if(file == null){
        getName = oldCover
    }else{
        getCover = req.files.file
        coverImage = getCover.name
        getCover.mv('./server/img/menu/' + getName)
        fs.unlinkSync(path.join(__dirname, '../img/menu/', oldCover))
        getName = `${randomId}_${coverImage.toLowerCase().replace(/ /g,'_')}`
    }

    await connection.query(`SELECT * FROM menu WHERE id ='${id}' `,
    (err, results, fields) => {
        var data = results
        if(data == 0){
            res.status(404).json({
                success: false,
                message: '404'})
        }else{
            connection.query(`UPDATE menu SET cover='${getName}', label='${label}',kategori_menu_id='${kategori_menu_id}',harga='${harga}',deskripsi='${deskripsi}' WHERE id ='${id}' `,
            (err, results, fields) => {
                res.json({
                    success: true,
                    message: 'updated success'
                })
            }) 
        }
    })
})

r.delete('/:id', async (req, res) => {
    var id = req.params.id
    connection.query(`SELECT * FROM menu WHERE id ='${id}' `,
    (err, results, fields) => {
        var data = results
        if(data == 0){
            res.json({
                success: false,
                message: '404'
            })
        }else{
                connection.query(`DELETE FROM gambar_menu WHERE menu_id ='${id}' `,
                    (err, results, fields) => {
                        for(let i = 0; i < results.length; i++){
                            fs.unlinkSync('/images/menu/'+ results[i].name )
                        }
                    console.log('berhasil hapus gambar')
                    connection.query(`DELETE FROM menu WHERE id ='${id}' `,
                    (err, results, fields) => {
                        console.log('berhasil hapus')
                        res.json({
                            success: true,
                            message: 'deleted success'
                        })
                    })
            })
        }
    })
})

// gambar
r.post('/images', (req,res) => {
    var randomId = randomInt(1, 9999);
    const getCover = req.files.file
    const coverImage = getCover.name
    var getName = `${randomId}_${coverImage.toLowerCase().replace(/ /g,'_')}`
    var menu_id = req.body.menu_id

    connection.query(`INSERT INTO gambar_menu VALUES('${randomId}','${getName}','${menu_id}')`,
    (err, results, fields) => {
        if(err){
            console.log(err)
            console.log(results)
            console.log(fields)
            res.json({
                success: false,
                message: 'failed'
            })
        }else{
            console.log(err)
            console.log(results)
            console.log(fields)
            getCover.mv('./server/img/menu/' + getName)
            res.json({
                success: true,
                message: 'success'
            })
        }
    }) 
})

r.get('/images',(req,res) => {
    connection.query(`SELECT DISTINCT a.* FROM gambar_menu as a 
    LEFT JOIN menu as c ON a.menu_id = c.id
	LEFT JOIN kategori_menu as b ON c.kategori_menu_id = b.id `,
    (err, results, fields) => {
        var payload = []
        results.map(a =>{
            payload.push({
                id  : a.id,
                name: a.nama,
                relation_id : a.menu_id,
                path: '/img/menu/'
            })
        })
        if(results == 0){
            res.json({
                success: false,
                message: 'data tidak tersedia!'
            })
        }else{
            res.send({
                success: true,
                data:payload
            })
        }
    }) 
})  

r.get('/images/detail-relation/:id',(req,res) => {
    var id = req.params.id
    connection.query(`SELECT DISTINCT a.* FROM gambar_menu as a 
    LEFT JOIN menu as c ON a.menu_id = c.id
    LEFT JOIN kategori_menu as b ON c.kategori_menu_id = b.id 
    WHERE menu_id = '${id}'
    `,
    (err, results, fields) => {
        var payload = []
        results.map(a =>{
            payload.push({
                id  : a.id,
                name: a.nama,
                relation_id : a.menu_id,
                path: '/img/menu/'
            })
        })
        if(results == 0){
            res.json({
                success: false,
                message: 'Not Found!'
            })
        }else{
            res.send({
                success: true,
                data:payload
            })
        }
    }) 
})  

r.delete('/images/:id', async (req, res) => {
    var id = req.params.id
    await connection.query(`SELECT * FROM gambar_menu WHERE id ='${id}' `,
    (err, results, fields) => {
        var data = results
        if(data == 0){
            res.json({
                success: false,
                message: '404'
            })
        }else{
            var name = results[0].nama
            fs.unlinkSync('/images/menu/'+ name )
            connection.query(`DELETE FROM gambar_menu WHERE id ='${id}' `,
            (err, results, fields) => {
                res.json({
                    success: true,
                    message: 'deleted success'
                })
            }) 
        }
    })
})

// kategori menu
r.get('/kategori', (req, res) => {
    connection.query(`SELECT * FROM kategori_menu `,
    (err, results, fields) => {
        console.log(results)
        if(results == 0){
            res.json({
                success: false,
                message: 'Not Found!',
                data:results
            })
        }else{
            res.send({
                success: true,
                data:results
            })
        }
    }) 
})

r.get('/kategori/:id', (req, res) => {
    var id = req.params.id
    connection.query(`SELECT * FROM kategori_menu WHERE id = '${id}' `,
    (err, results, fields) => {
        console.log(results)
        if(results == 0){
            res.json({
                success: false,
                message: 'Not Found!'
            })
        }else{
            res.send({
                success: true,
                data:results
            })
        }
    }) 
})

r.post('/kategori', (req, res) => {
    var {
        label
    } = req.body
    connection.query(`INSERT INTO kategori_menu VALUES(NULL,'${label}')`,
    (err, results, fields) => {
        if(err){
            console.log(err)
            res.json({
                success: false,
                message: 'failed'
            })
        }else{
            res.json({
                success: true,
                message: 'success'
            })
        }
    }) 
})

r.patch('/kategori/:id', async (req, res) => {
    var id = req.params.id
    var label = req.body.label

    await connection.query(`SELECT * FROM kategori_menu WHERE id ='${id}' `,
    (err, results, fields) => {
        var data = results
        if(data == 0){
            res.json({
                success: false,
                message: '404'
            })
        }else{
            connection.query(`UPDATE kategori_menu SET label='${label}' WHERE id ='${id}' `,
            (err, results, fields) => {
                res.json({
                    success: true,
                    message: 'updated success'
                })
            }) 
        }
    })
})

r.delete('/kategori/:id', async (req, res) => {
    var id = req.params.id
    await connection.query(`SELECT * FROM kategori_menu WHERE id ='${id}' `,
    (err, results, fields) => {
        var data = results
        if(data == 0){
            res.json({
                success: false,
                message: '404'
            })
        }else{
            connection.query(`DELETE FROM kategori_menu WHERE id ='${id}' `,
            (err, results, fields) => {
                res.json({
                    success: true,            
                    message: 'deleted success'
                })
            }) 
        }
    })
})

module.exports = r
