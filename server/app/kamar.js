const express = require('express')
const r = express.Router()
const connection = require('./../util/index')
const randomInt = require('random-int');
const fs = require('fs')

//kamar
r.get('/', (req, res) => {
    connection.query(`SELECT a.id, a.*, b.label as kategori FROM kamar as a 
    LEFT JOIN kategori_kamar as b ON a.kategori_kamar_id = b.id 
    ORDER BY a.nama`,
    (err, results, fields) => {
        if(err){
            res.json({
                success: false,
                message: 'Not Found!'
            })
        }else{
            var payload = []
            results.map(a => {
                var getStatus = ''
                if(a.status == 1){
                    getStatus = 'Tidak Tersedia'
                }else{
                    getStatus = 'Tersedia'
                }
                payload.push({
                    id                  : a.id,
                    nama                : a.nama,
                    kategori_kamar_id   : a.kategori_kamar_id,
                    status              : getStatus,
                    kategori            : a.kategori
                })
            })
            
            return res.status(200).json({
                success: true,
                data:payload
            })
        }
    })

})

r.get('/tersedia', (req, res) => {
    connection.query(`SELECT a.id, a.*, b.label as kategori FROM kamar as a 
    LEFT JOIN kategori_kamar as b ON a.kategori_kamar_id = b.id 
    WHERE a.status = 0
    ORDER BY a.nama`,
    (err, results, fields) => {
        if(err){
            res.json({
                success: false,
                message: 'Not Found!'
            })
        }else{
            var payload = []
            results.map(a => {
                var getStatus = ''
                if(a.status == 1){
                    getStatus = 'Tidak Tersedia'
                }else{
                    getStatus = 'Tersedia'
                }
                payload.push({
                    id                  : a.id,
                    nama                : a.nama,
                    kategori_kamar_id   : a.kategori_kamar_id,
                    status              : getStatus,
                    kategori            : a.kategori
                })
            })
            
            return res.status(200).json({
                success: true,
                data:payload
            })
        }
    })

})

r.get('/detail/:id', async (req, res) => {
    var id = req.params.id
    await connection.query(`SELECT * FROM kamar WHERE id ='${id}' `,
    (err, results, fields) => {
            if(results == 0){
                res.json({message: '404'})
            }else{
            connection.query(`SELECT a.*, b.id as rls_id, b.label as rls_label FROM kamar as a 
            LEFT JOIN kategori_kamar as b ON a.kategori_kamar_id = b.id WHERE a.id = '${id}' `,
            (err, results, fields) => {
                console.log(results)
                var a = results[0]
                var payload = {
                    id          : a.id,
                    nama        : a.nama,
                    kategori    : a.rls_label,
                    kategori_id : a.kategori_kamar_id,
                    status      : a.status,
                }
                
                if(results == 0){
                    res.json({
                        success: false,
                        message: 'Not Found!'
                    })
                }else{
                    res.send({
                        success: true,
                        data:payload
                    })
                }
            }) 
        }
    })
})

r.post('/', (req,res) => {
    var {
        nama,
        kategori_kamar_id,
        status
    } = req.body

    connection.query(`SELECT * FROM kamar WHERE nama = '${nama}' `,
    (err, results, fields) => {
        if(results == 0){
            connection.query(`INSERT INTO kamar VALUES(NULL,'${nama}','${kategori_kamar_id}',0)`,
            (err, results, fields) => {
                console.log(err)
                console.log(results)
                console.log(fields)
                if(err){
                    res.json({
                        success: false,
                        message: 'failed'
                    })
                }else{
                    res.json({
                        success: true,
                        message: 'tambah kamar berhasil'
                    })
                }
            }) 
        }else{
            res.json({message: 'kamar sudah terdaftar'})
        }
    }) 

})

r.patch('/:id', async (req, res) => {
    var id = req.params.id
    var {
        nama,
        kategori_kamar_id
    } = req.body

    await connection.query(`SELECT * FROM kamar WHERE id ='${id}' `,
    (err, results, fields) => {
        var data = results
        if(data == 0){
            res.json({
                success: false,
                message: '404'
            })
        }else{
            connection.query(`UPDATE kamar SET nama='${nama}',kategori_kamar_id='${kategori_kamar_id}'  WHERE id ='${id}' `,
            (err, results, fields) => {
                res.json({
                    success: true,
                    message: 'ubah data berhasil'
                })
            }) 
        }
    })
})

r.delete('/:id', async (req, res) => {
var id = req.params.id
await connection.query(`SELECT * FROM kamar WHERE id ='${id}' `,
(err, results, fields) => {
    var data = results
    if(data == 0){
        res.json({
            success: true,
            message: '404'
        })
    }else{
        connection.query(`DELETE FROM kamar WHERE id ='${id}' `,
        (err, results, fields) => {
            res.json({
                success: true,
                message: 'berhasil menghapus data'
            })
        })
    }
})
})

// gambar
r.post('/images', (req,res) => {
    var randomId = randomInt(1, 9999);
    const getCover = req.files.cover
    const coverImage = getCover.name
    var getName = `${randomId}_${coverImage.toLowerCase().replace(/ /g,'_')}`
    var kamar_id = req.body.kamar_id

    connection.query(`INSERT INTO gambar_kamar VALUES(NULL,'${getName}','${kamar_id}')`,
    (err, results, fields) => {
        if(err){
            res.json({
                success: false,
                message: 'failed'
            })
        }else{
            getCover.mv('./server/img/kamar/' + getName)
            res.json({
                success: true,
                message: 'success'
            })
        }
    }) 
})

r.get('/images',(req,res) => {
    connection.query(`SELECT DISTINCT a.*, b.label as kategori FROM gambar_kamar as a 
	LEFT JOIN kategori_kamar as b ON a.kamar_id = b.id `,
    (err, results, fields) => {
        var payload = []
        results.map(a =>{
            payload.push({
                id  : a.id,
                name: a.nama,
                kategori_kamar : a.kategori,
                path: '/img/kamar/'
            })
        })
        if(results == 0){
            res.json({
                success: false,
                message: 'Not Found!'
            })
        }else{
            res.send({
                success: true,
                data:payload
            })
        }
    }) 
})  

r.get('/images/detail-relation/:id',(req,res) => {
    var id = req.params.id
    connection.query(`SELECT DISTINCT a.*, b.label as kategori FROM gambar_kamar as a 
    LEFT JOIN kategori_kamar as b ON a.kamar_id = b.id
    LEFT JOIN kamar as c ON a.kamar_id = c.id
    WHERE b.id = '${id}'
    `,
    (err, results, fields) => {
        var payload = []
        results.map(a =>{
            payload.push({
                id  : a.id,
                name: a.nama,
                kategori_kamar : a.kategori,
                path: '/img/kamar/'
            })
        })
        if(results == 0){
            res.json({
                success: false,
                message: 'Not Found!'
            })
        }else{
            res.send({
                success: true,
                data:payload
            })
        }
    }) 
})  

r.delete('/images/:id', async (req, res) => {
    var id = req.params.id
    await connection.query(`SELECT * FROM gambar_kamar WHERE id ='${id}' `,
    (err, results, fields) => {
        var data = results
        if(data == 0){
            res.json({
                success: false,
                message: '404'
            })
        }else{
            var name = results[0].nama
            // fs.unlinkSync('/images/menu/'+ name )
            connection.query(`DELETE FROM gambar_kamar WHERE id ='${id}' `,
            (err, results, fields) => {
                res.json({
                    success: true,
                    message: 'hapus gambar berhasil'
                })
            }) 
        }
    })
})

// kategori kamar
r.get('/kategori', (req, res) => {
    connection.query(`SELECT * FROM kategori_kamar `,
    (err, results, fields) => {
        console.log(results)
        if(results == 0){
            res.json({
                success: false,
                message: 'Not Found!',
                data:results
            })
        }else{
            res.send({
                success: true,
                data:results
            })
        }
    }) 
})

r.get('/kategori/:id', (req, res) => {
    var id = req.params.id
    connection.query(`SELECT * FROM kategori_kamar WHERE id = '${id}' `,
    (err, results, fields) => {
        console.log(results)
        if(results == 0){
            res.json({
                success: false,
                message: 'Not Found!'
            })
        }else{
            res.send({
                success: true,
                data:results
            })
        }
    }) 
})

r.post('/kategori', (req, res) => {
    var {
        label
    } = req.body
    connection.query(`INSERT INTO kategori_kamar VALUES(NULL,'${label}')`,
    (err, results, fields) => {
        if(err){
            res.json({
                success: false,
                message: 'failed'
            })
        }else{
            res.json({
                success: true,
                message: 'success'
            })
        }
    }) 
})

r.patch('/kategori/:id', (req, res) => {
    var id = req.params.id
    var label = req.body.label

    connection.query(`SELECT * FROM kategori_kamar WHERE id ='${id}' `,
    (err, results, fields) => {
        var data = results
        if(data == 0){
            res.json({
                success: false,
                message: '404'
            })
        }else{
            connection.query(`UPDATE kategori_kamar SET label='${label}' WHERE id ='${id}' `,
            (err, results, fields) => {
                res.json({
                    success: true,
                    message: 'updated success'
                })
            }) 
        }
    })
})

r.delete('/kategori/:id', async (req, res) => {
    var id = req.params.id
    connection.query(`SELECT * FROM kategori_kamar WHERE id ='${id}' `,
    (err, results, fields) => {
        var data = results
        if(data == 0){
            res.json({
                success: false,
                message: '404'
            })
        }else{
                connection.query(`DELETE FROM gambar_kamar WHERE kamar_id ='${id}' `,
                (err, results, fields) => {
                    console.log(results.affectedRows)
                    if(results.affectedRows == 0){
                        connection.query(`DELETE FROM kategori_kamar WHERE id ='${id}' `,
                        (err, results, fields) => {
                            console.log(err)
                            console.log(results)
                            console.log(fields)
                            console.log('berhasil hapus ?')
                            res.json({
                                success: true,
                                message: 'deleted success'
                            })
                        })
                    }else{
                        console.log('berhasil hapus gambar')
                        connection.query(`DELETE FROM kategori_kamar WHERE id ='${id}' `,
                        (err, results, fields) => {
                            for(let i = 0; i < results.length; i++){
                                fs.unlinkSync('/images/kamar/'+ results[i].name )
                            }
                            console.log('berhasil hapus')
                            res.json({
                                success: true,
                                message: 'deleted success'
                            })
                        })
                    }

                    
                
            })
        }
    })
})

module.exports = r
