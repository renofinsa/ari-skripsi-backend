const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const uploads = require('express-fileupload')
const path = require('path')
var cookieParser = require('cookie-parser')
const SECRET = process.env.SECRET || 'asjd9x8cuz9xiuc01id9ixmozicn982klkczkc'
app.use(cookieParser(SECRET))


app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))
app.use(uploads())
// img routes
app.use('/images/menu', express.static(path.join(__dirname, '../', '/server/img/menu')))
app.use('/images/kamar', express.static(path.join(__dirname, '../', '/server/img/kamar')))




const api = express.Router()
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});

// set routes url
app.use('/api/v1', api)
// routes
api.use('/', require('./app/index'))
api.use('/auth', require('./app/auth'))
api.use('/menu', require('./app/menu'))
api.use('/kamar', require('./app/kamar'))
api.use('/reservasi', require('./app/reservasi'))
api.use('/order', require('./app/order'))
// api.use('/', require('./server'))
// routes


const port = process.env.PORT || 3002
app.listen(port, () => console.log(`App listening on port ${port}!`))